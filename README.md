# Fondements de la programmation - Méga résumé (et notes de cours) pour révision++

Mes notes se basent sur le cours de Monsieur Boudes, qui est basé en grande partie sur les livres Practical Foundations of Programming Languages de Robert Harper et Types and Programming languages de Benjamin Pierce. Le reste est plus de l'ordre d'une discussion générale au sujet des langages de programmation.

Une grande partie de mes notes est un simple ctrl-c ctrl-v remixé

## Sources : 
- Fondements de la programmation, Pierre Boudes, P. Jacobé de Naurois, LIPN https://gitlab.sorbonne-paris-nord.fr/boudes/cours-fondementsprog-m1
- Logique et Preuves, Pierre Castéran, LaBRI https://www.labri.fr/perso/casteran/L-et-P/poly.pdf (Poly externe sur les séquents)
- Practical Foundations for Programming Languages, Robert Harper https://www.cs.cmu.edu/~rwh/pfpl/2nded.pdf
- Tony Hoare, The billion dollar mistake, https://www.infoq.com/presentations/Null-References-The-Billion-Dollar-Mistake-Tony-Hoare/
- http://pauillac.inria.fr/~cheno/refcaml.pdf
- https://www2.lib.uchicago.edu/keith/ocaml-class/definitions.html
- https://fr.wikiversity.org/wiki/Premiers_pas_en_OCaml/Filtrage_de_motif#:~:text=Le%20filtrage%20de%20motif%20s,associ%C3%A9e%20une%20expression%20%C3%A0%20retourner.
- https://latex.codecogs.com/ (Outil utilisé pour faire les formules mathématiques en Latex)
- https://www.fil.univ-lille1.fr/~wegrzyno/portail/Elfe/Doc/Cours-PF/cours-4bis.pdf
- https://www.lri.fr/~filliatr/ens/compil/repro/ocaml.pdf

## Quelques rappels sur les séquents et la logique 
- n nat : n est un entier naturel
- n = n1 + n2 : n est la somme de n1 et de n2
- τ Typ = τ est un type
- e = une expression
- e : τ = l'expression est de type τ
- Γ = Ensembles de propositions appelées prémisses (c'est une proposition, une affirmation avancée en support à une conclusion) ou hypothèses
- ⊢ = thèse, turnstile : déduction, prouvable
- V = OR
- Γ, A = Adjonction d’une hypothèse A à un contexte Γ
- Γ, ∆ = Union de deux contextes

Dans un séquent, la structure est de cette forme : 
- Contexte formé d'ensemble **Γ** de propositions prémisses  
- Une propostion A appelée conclusion

Ainsi, nous obtenons : [![forme](https://latex.codecogs.com/svg.image?%5CGamma%20:%20A)]()

#### Exemples : 

[![ex1](https://latex.codecogs.com/svg.image?x&space;:&space;y)]()
> y est déductible de x 

> On peut déduire y grâce à x

Les hypothèses sont séparées par des virgules : [![ex2](https://latex.codecogs.com/svg.image?A_{1},...,A_{n}&space;\vdash&space;A)]()
 où les Ai sont les prémisses et A la conclusion et se lit ainsi : 

- Sous les hypothèses A<sub>1</sub>,...,A<sub>n</sub>, la conclusion A est vraie
- La proposition A est une conséquence logique de A<sub>1</sub>,...,A<sub>n</sub>
- Si A<sub>1</sub>,...,A<sub>n</sub> sont vraies, alors A est vraie

Ce séquent affirme que si x est un nombre réel vérifiant l'égalité 2x<sup>2</sup>-5x+2 = 0, alors x = 2 et, nous avons deux prémisses dans ce séquent

[![ex3](https://latex.codecogs.com/svg.image?x\in\mathbb{R},2x^2-5x&plus;2&space;=&space;0&space;\vdash&space;x&space;=&space;2
)]()

Mais ne peut pas être tenu pour vrai, si on prend x = 1/2, les hypothèses sont vérifiées alors que la conclusion est fausse.

Solution :  Il faut affaiblir -> [![ex3](https://latex.codecogs.com/svg.image?\Gamma&space;\vdash&space;x&space;=&space;2&space;\vee&space;\frac{1}{2})]() 

Contexte sans prémisses : ⊢ A -> La proposition A est notée vraie (sans conditions)

-> Un séquent est valide si toutes les hypothèses sont vraies alors la conclusion est vraie.

## Quelques rappels sur la programmation vu en cours 

### Paradigmes de programmation

| Concepts | Paradigmes |Langages |
| ------ | ------ |------ |
|  Variables impératives, mutables, valeur avec nom | Programmation impérative : séquence clairement définie d’instructions informatiques |OCAML, Haskell, C... |
| routine, sous-routine ou fonction, effet de bord | Programmation procédurale : appel de procédures,  |PL SQL, C, Ada, Fortran |
|blabla  | Programmation fonctionnelle  |F#, OCAML, Scala, Haskell, ML |
|Objets, héritage, encapsulation | Programmation objet |C++, C#, Java |

Il y'a aussi logique et concurrencielle 


### Fonctions lambda : 
- C ne possède pas pas de lambdas
- Java, Python, C# possèdent des lambdas

#### Tuples : 
```python 
[("Institut Galilée", "Villetaneuse", 'C', 316)]
```

#### Enregistrements : 
```javascript 
{
    "Ecole":"Institut Galilée",
    "Commune" : "Villetaneuse",
    "Batiment": "C",
    "Numéro de la salle" : 316
}
```
#### Prototypes en Self (Ancètre du JavaScript) : 
```javascript
x.a; // a non défini
// mais
x.prototype = y; 
y.a // a défini dans y
// alors
x.a // appellera y.a
``` 

Réference à lire pour la culture gé de développeur : Alan Kay,  [What is Alan Kay's definition of Object Oriented?](https://www.quora.com/What-is-Alan-Kays-definition-of-Object-Oriented) sur Quora.

- Type Produit = A<sub>1</sub> * A<sub>2</sub>
- Type somme = A<sub>1</sub> + A<sub>2</sub>
- Type Union = Soit un entier, soit un caractère. Soit l'un, soit l'autre. -> En Scala, on utilise EIther
https://blog.engineering.publicissapient.fr/2012/04/05/les-types-monadiques-de-scala-le-type-either/
- Type Option = utilisé lorsqu’il est possible qu’il n’existe pas de valeur réelle pour une valeur ou une variable nommée. Une option a un type sous-jacent et peut contenir une valeur de ce type, ou elle peut ne pas avoir de valeur.
https://docs.microsoft.com/fr-fr/dotnet/fsharp/language-reference/options

En F# :
```F#
let keepIfPositive (a : int) = if a > 0 then Some(a) else None
```

- Si la valeur est nulle, n'existe pas ou est invalide -> None est appelé 
- Si la valeur est valide et existe, Some est appelé et donne la valeur à l'option

En Java -> Optional<T> / @Nullable @NotNull

En C++ -> std::Optional<T>, Optional()

En C# -> Nullable<T>

En OCAML -> type 'a option = None | Some of 'a.

Avantage : Permet d'éviter le NullPointerException

Autre référence à lire -> Tony Hoare, The billion dollar mistake

Programme paralèlle / Programmation concaténative 

```javascript
List(1,2,3).map(f).filter()
List(1,2,3).map(_*2).filter(_%3 == 0)  // On ne garde que les multiples de 3
// _ : Paramètre qui contient la valeur du tableau à chaque position
```

Programmation paralèlle : programme divisé en plusieurs tâches s'exécutant simultanément

## Notre langage EF (à mi semestre)

### Les types :
![image-1.png](./image-1.png)

### Les expressions & rappels OCAML
![image-3.png](./image-3.png)
### Tuples
![image-5.png](./image-5.png)

Le type Unit ne contient qu'un élément noté (), utilisé pour les fonctions "sans arguents" -> seul argument (). Egalement utilisé pour les fontions qui ne "renvoient rien" en OCAML.
### Projection 
![image-6.png](./image-6.png)

Multiplets, nommage des projections, appelés des champs
```ocaml
type individu = { Nom : string ; Prenom : string ; Secu : int }
```
### Définition 
![image-7.png](./image-7.png)

```ocaml
let name = expr1 in expr2

let n = 2 in n * n;;
    (*- : int = 4*)
let list = ["foo";"bar";"baz"] in List.hd list;;
    (*- : string = "foo"*)
let a = 2 in
    let b = 3 in
	  a + b
        ;;
    (*- : int = 5*)
```

### Application 
![image-8.png](./image-8.png)
### Fonction anonyme λ 
![image-4.png](./image-4.png)

peut s'écrire comme ça en syntaxe réelle λ(x:τ) => e

```ocaml
let carre_de_somme a b = (function x -> x * x) (a + b) ;;
(*fonction anonyme suivante renvoie le carré de la somme de ses arguments*)

(*En nommant explicitement la fonction : *)
let carre x = x * x ;;
let carre_de_somme a b = carre (a + b) ;;
```
https://fr.wikipedia.org/wiki/Fonction_anonyme

### Match 
![image-9.png](./image-9.png)

Le filtrage de motif permet soit de gérer différents cas en fonction des valeurs d'une expression.

```ocaml
match Random.int 6 with
    | 0 -> "chou-fleur"
    | 1 -> "cerise"
    | 2 -> "chocolat"
    | _ -> "poivre"
    ;;
(*- : string = "chou-fleur"*)
```
### Injection
![image-10.png](./image-10.png)

### Opératateur de point fixe

![image-11.png](./image-11.png)

Utilisé pour la récursivité.
letrec en ocaml
(fix f) -> f(fix f) = fix donne une copie la fonction à elle même
```ocaml
letrec modulo : (num, num) -> num =
    λ(p: (num, num)) (*ou fun(p: (num, num*)
        let a = p1(p) in 
            let b = p2(p) in
        ifpos (a-b) then modulo((a-b, b)) (*ou ifpos (p1-p2) then modulo(p1-p2, p2) else p1*)
        else a (*modulo deviant g*)
in monProgramme;

(*avec un opérateur de point fixe*)
let modulo = fix λ(g: (num, num) -> num) λ(p: (num, num))
    let a = p1(p) in
        let b = p2(p) in
            ifpos (a-b) then g((a-b, b))
            else a
in modulo((5, 3);
```
### Test
![image-12.png](./image-12.png)


## Typage 
- Un jugement de typage [![forme](https://latex.codecogs.com/svg.image?\Gamma&space;\vdash&space;e&space;:\tau)]() **permet d'associer un type τ à une expression e dans un contexte Γ**.

- Le contexte Γ décrit un ensemble de variables dom(Γ) et le choix d'un type pour chacune de ces variables

- Un jugement de typage est valide s'il peut être dérivé grâce aux règles suivantes : 

### Règles

![image-13.png](./image-13.png)

#### Typer 

Est-ce que ces expressions sont-elles typables ? Il faut le prouver avec les règles de typage :

1. Exp 1 : 
```ocaml
let x = "hello" in x ^ x
```

##### Étape 1 : Passage en syntaxe de surface
On passe l'expression en syntaxe de surface :
![image-15.png](./image-15.png)

/!\ On remplace "hello" par ["hello"]
##### Étape 2 : On dérive à la règle suivante l'expression e1 ou "hello"
On obtiens le τ<sub>1<sub> = str :
![image-16.png](./image-16.png)

##### Étape 3 : On dérive à la règle suivante l'expression e2 ou x.cat(x,x)
![image-18.png](./image-18.png)

##### Étape 4 : On dérive aux règles suivantes pour les expressions e1 et e2 de cat (les x)
![image-20.png](./image-20.png)

##### Résultat : 

![image-22.png](./image-22.png)

2. Exp 2 : 
```ocaml
let x = 2 in 10 + x
```

##### Résultat : 
![image-23.png](./image-23.png)

3. Exp 3 : 
```ocaml
"hello" + 10
```
![image-24.png](./image-24.png)


4. Exp 4 : 
```ocaml
let x = "hello" in 10 + x

Erreur : x : str 
```
![image-25.png](./image-25.png)

5. Exp 5 : 
```ocaml
let x = = y ^ y in len(x) + z
```
![image-26.png](./image-26.png)

## Preuves des lemmes

### Unicité du typage (lemme 4.1)
Dans ce système de types, pour toute expression e, pour tout contexte Γ il y'a au plus un type τ tel que **Γ ⊢ e : τ**

### Inversion du typage (lemme 4.2)

- Toute expression du langage conrespond à unique règle de typage 
- Pour qu'une expression soit typable, il est nécessaire que ses sous-expressions satisfassent les contraintes données par la règle de typage

Example : 
- Pour que Γ ⊢ cat(e<sub>1</sub>, e<sub>2</sub>) : τ, il faut que τ = str et Γ ⊢ e<sub>i</sub> : str (i = 1,2)


### Affaiblissement (lemme 4.3)

- On peut toujours ajouter une hypothèse de typage sur une variable fraîche à une dérivation de type.







